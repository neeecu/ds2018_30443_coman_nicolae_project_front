export class User {
  userId: number;
  username: string;
  password: string;
  email: string;
  currentIncome: number;
  userRole: string;
  budgetIds: number[];
}
