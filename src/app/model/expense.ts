export class Expense {
  expenseId: number;
  budgetId: number;
  expenseType: string;
  value: number;

}
