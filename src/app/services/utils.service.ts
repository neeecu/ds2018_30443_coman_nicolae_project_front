import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {JwtToken} from '../model/jwtToken';
import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {User} from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  loginURL = 'http://localhost:8080/api/auth/authenticate';
  registerURL = 'http://localhost:8080/api/register';

  constructor(private http: HttpClient, private router: Router) {
  }

  doLogin(userDetails: string) {
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    const options = {headers: headers};
    return this.http.post<JwtToken>(this.loginURL, userDetails, options);
  }

  register(form: NgForm) {
    let regData = {
      username: '',
      email: '',
      income: '',
      password: '',
      token: '',
    };

    regData = form.value;


    const dto = {
      username: regData.username,
      password: regData.password,
      email: regData.email,
      currentIncome: regData.income


    };
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    const options = {headers: headers};
    return this.http.post<User>(this.registerURL + '?token=' + regData.token, dto, options);

  }

  doLogOut() {
    this.router.navigate(['']);
    if (localStorage.getItem('role') === 'ROLE_USER') {
      const budgetsButton = document.getElementById('viewBudgetsBtn');
      budgetsButton.style.display = 'none';
    } else {
      const usersButton = document.getElementById('viewUsersBtn');
      usersButton.style.display = 'none';
    }
    localStorage.clear();
    const loginButton = document.getElementById('loginButton');
    loginButton.style.display = 'block';
    const logoutButton = document.getElementById('logoutButton');
    logoutButton.style.display = 'none';
    const registerButton = document.getElementById('registerButton');
    registerButton.style.display = 'block';
  }

}
