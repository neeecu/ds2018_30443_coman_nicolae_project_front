import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {User} from '../model/user';
import {JwtToken} from '../model/jwtToken';
import {RegToken} from '../model/regToken';
import {NgForm} from '@angular/forms';


@Injectable({
  providedIn: 'root'
})
export class UserService {

  usersURL = 'http://localhost:8080/api/user';

  constructor(private http: HttpClient) {
  }

  getUsers() {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.get<User[]>(this.usersURL + '/all', options);
  }

  getUserById(id: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.get<User>(this.usersURL + '/' + id + '/get', options);
  }

  removeUser(id: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.get<User>(this.usersURL + '/' + id + '/delete', options);
  }

  updateUser(id: number, user: User) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken'),
      'Content-Type': 'application/json'
    });
    const options = {headers: headers};
    return this.http.post<User>(this.usersURL + '/' + id + '/update', user, options);
  }

  addUser(user: User) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken'),
      'Content-Type': 'application/json'
    });
    const options = {headers: headers};
    return this.http.post<User>(this.usersURL + '/save', user, options);

  }

  sendToken(params: NgForm) {

    let body = {
      destEmail: '',
      isAdmin: null
    };

    body = params.value;
    if (body.isAdmin === '') {
      body.isAdmin = false;
    }
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken'),
      'Content-Type': 'application/json',
    });
    const options = {headers: headers};
    return this
      .http
      .post<RegToken>('http://localhost:8080/api/sendToken' + '?destEmail=' + body.destEmail + '&isAdmin=' + body.isAdmin
        , null, options);
  }

}
