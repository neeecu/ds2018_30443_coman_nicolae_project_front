import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Budget} from '../model/budget';
import {Form, NgForm} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class BudgetService {
  budgtetsUrl = 'http://localhost:8080/api/budget';

  constructor(private http: HttpClient) {
  }

  getBudgets(userId: string) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.get<Budget[]>(this.budgtetsUrl + '/' + userId + '/all', options);
  }

  addBudget(income: any, userId: number) {
    let formValue = {
      income: '',
      budgetName: ''
    };
    formValue = income;
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.post<Budget>(this.budgtetsUrl + '/create/' + formValue.income + '?userId=' + userId, null, options);
  }

  updateBudget(budget: Budget) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.post<Budget>(this.budgtetsUrl + '/' + budget.budgedId + '/update', budget, options);
  }

  removeBudget(budgetId: number, userId: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.post<Budget>(this.budgtetsUrl + '/' + budgetId + '/delete' + '?userId=' + userId, null, options);
  }

  getReport(budgetId: number) {
    const headers = new HttpHeaders({
      'Authorization': 'Bearer ' + localStorage.getItem('authToken')
    });
    const options = {headers: headers};
    return this.http.get<Map<string, number>>(this.budgtetsUrl + '/' + budgetId + '/getReport', options);
  }

}
