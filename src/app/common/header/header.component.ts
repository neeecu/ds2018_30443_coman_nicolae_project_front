import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {UtilsService} from '../../services/utils.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private router: Router, private utilsService: UtilsService) {
  }

  ngOnInit() {
    const role = localStorage.getItem('role');

    const userButton = document.getElementById('viewUsersBtn');
    const budgetsButton = document.getElementById('viewBudgetsBtn');
    const logoutButton = document.getElementById('logoutButton');
    const loginButton = document.getElementById('loginButton');
    const regButton = document.getElementById('registerButton');

    budgetsButton.style.display = 'none';
    userButton.style.display = 'none';
    loginButton.style.display = 'block';
    logoutButton.style.display = 'none';
    regButton.style.display = 'block';

    if (role === 'ROLE_USER') {
      budgetsButton.style.display = 'block';
      loginButton.style.display = 'none';
      logoutButton.style.display = 'block';
      regButton.style.display = 'none';

    }

    if (role === 'ROLE_ADMIN') {
      userButton.style.display = 'block';
      loginButton.style.display = 'none';
      logoutButton.style.display = 'block';
      regButton.style.display = 'none';

    }


  }

  goToUsers() {
    this.router.navigate(['/users']);
  }

  goToRegister() {
    this.router.navigate(['register']);
  }

  goHome() {
    this.router.navigate(['']);
  }

  goToLogin() {
    this.router.navigate(['login']);

  }

  doLogout() {
    this.utilsService.doLogOut();
    localStorage.clear();
  }

  goToBudgets() {
    this.router.navigate(['budgets']);
  }
}
