import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';
import {JwtToken} from '../../model/jwtToken';
import {UtilsService} from '../../services/utils.service';
import {HttpErrorResponse} from '@angular/common/http';
import {TrackerException} from '../../model/exception';
import swal from 'sweetalert';


@Component({
  selector: 'app-users',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  authToken: JwtToken = null;
  loginButton = document.getElementById('loginButton');
  registerButton = document.getElementById('registerButton');
  logoutButton = document.getElementById('logoutButton');
  errorResponse: HttpErrorResponse;

  constructor(private router: Router, private utilsService: UtilsService) {
  }

  receivedException: TrackerException;
  errorBody: HttpErrorResponse;

  ngOnInit() {
  }

  onSubmit(form: NgForm) {
    if (localStorage.getItem('authToken') == null) {
      this.utilsService.doLogin(JSON.stringify(form.value)).subscribe(data => {
          this.authToken = data;
          localStorage.setItem('authToken', this.authToken.token);
          localStorage.setItem('role', this.authToken.role);
          localStorage.setItem('userId', this.authToken.userId);
          if (this.authToken != null) {
            this.loginButton.style.display = 'none';
            this.registerButton.style.display = 'none';
            this.logoutButton.style.display = 'block';

            if (this.authToken.role === 'ROLE_ADMIN') {
              const usersButton = document.getElementById('viewUsersBtn');
              usersButton.style.display = 'block';
              this.router.navigate(['users']);

            }

            if (this.authToken.role === 'ROLE_USER') {
              const budgetsButton = document.getElementById('viewBudgetsBtn');
              budgetsButton.style.display = 'block';
              this.router.navigate(['budgets']);
            }

          }
        },
        error => {
          this.errorBody = error;
          this.receivedException = this.errorBody.error;
          swal('Bummer', JSON.stringify(this.receivedException), 'error');});
    } else {
      alert('You are already logged in');
    }
  }

}
