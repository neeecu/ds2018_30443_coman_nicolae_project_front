import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {MatIconModule, MatMenuModule, MatToolbarModule, MatButtonModule, MatTableModule, MatDialogModule} from '@angular/material';
import {RouterModule} from '@angular/router';
import {ChartsModule} from 'ng2-charts/ng2-charts';

import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UsersComponent} from './admin/users/users.component';
import {HomeComponent} from './common/home/home.component';
import {HttpClientModule} from '@angular/common/http';
import {HeaderComponent} from './common/header/header.component';
import {LoginComponent} from './common/login/login.component';
import {FormsModule} from '@angular/forms';
import {RegisterComponent} from './common/register/register.component';
import {Error401Component} from './common/error/401/error-401-component.component';
import {Error404Component} from './common/error/404/error-404.component';
import {BudgetsComponent} from './user/budgets/budgets.component';
import {ExpensesComponent} from './user/expenses/expenses.component';

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    HomeComponent,
    HeaderComponent,
    LoginComponent,
    RegisterComponent,
    Error401Component,
    Error404Component,
    BudgetsComponent,
    ExpensesComponent

  ],
  imports: [
    ChartsModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot([
        {
          path: 'users',
          component: UsersComponent
        },
        {
          path: 'login',
          component: LoginComponent
        },
        {
          path: 'register',
          component: RegisterComponent
        },
        {
          path: 'expensesOf/:id',
          component: ExpensesComponent
        },
        {
          path: '',
          component: HomeComponent
        },
        {
          path: '401',
          component: Error401Component
        },
        {
          path: 'budgets',
          component: BudgetsComponent
        },
        {
          path: '**',
          component: Error404Component
        }
      ],
      {onSameUrlNavigation: 'reload'})],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
