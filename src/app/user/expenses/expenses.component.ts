import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Expense} from '../../model/expense';
import {ExpenseService} from '../../services/expense.service';
import {HttpErrorResponse} from '@angular/common/http';
import {TrackerException} from '../../model/exception';
import swal from 'sweetalert';

@Component({
  selector: 'app-users',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {

  budgetId: string;
  expenses: Expense[] = [];
  errorBody: HttpErrorResponse;
  receivedException: TrackerException;


  displayedColumns: string[] = ['expenseId', 'type', 'value', 'remove'];

  constructor(private route: ActivatedRoute, private expenseService: ExpenseService, private router: Router) {

  }


  ngOnInit() {
    if (localStorage.getItem('role') !== 'ROLE_USER') {
      this.router.navigate(['401']);
    } else {
      this.budgetId = this.route.snapshot.paramMap.get('id');
      console.log(this.budgetId);
      this.expenseService.getExpenses(this.budgetId).subscribe(data => this.expenses = data, error => {
        this.errorBody = error;
        this.receivedException = this.errorBody.error;
      });
    }
  }

  updateData() {
    this.expenseService.getExpenses(this.budgetId).subscribe(data => this.expenses = data, error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });
  }

  removeExpense(expense: Expense) {
    this.expenseService.removeExpense(this.budgetId, expense.expenseId).subscribe(() => this.updateData(), error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });
  }

  goBack() {
    window.history.back();
  }

}
