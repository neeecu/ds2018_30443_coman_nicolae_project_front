import {Component, OnInit, TemplateRef} from '@angular/core';
import {User} from '../../model/user';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {MatDialog} from '@angular/material';
import {NgForm} from '@angular/forms';
import {TrackerException} from '../../model/exception';
import {HttpErrorResponse} from '@angular/common/http';
import swal from 'sweetalert';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  users: User[] = [];
  displayedColumns: string[] = ['userId', 'username', 'email', 'currentIncome', 'userRole', 'remove'];
  clickedUser: User = null;
  receivedException: TrackerException;
  errorBody: HttpErrorResponse;

  constructor(private router: Router, private userService: UserService, private dialog: MatDialog) {
  }

  ngOnInit() {
    if (localStorage.getItem('role') !== 'ROLE_ADMIN') {
      this.router.navigate(['401']);
    } else {
      this.userService.getUsers()
        .subscribe(data => {
          this.users = data;
        }, error => {
          this.errorBody = error;
          this.receivedException = this.errorBody.error;
          swal('Bummer', this.receivedException.message, 'error');
        });
    }
  }

  viewDetails(id: number): void {
    this.router.navigate(['admin/user', id]);
  }


  showUser(templateRef, user: User) {
    this.clickedUser = user;
    const dialogRef = this.dialog.open(templateRef, {
      height: '420px',
      width: '300px',
    });
  }

  onSubmitUpdate(form: NgForm) {
    this.userService.updateUser(this.clickedUser.userId, form.value).subscribe(() => this.updateData(), error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', this.receivedException.message, 'error');
    });
  }

  showDialog(templateRef) {
    const dialogRef = this.dialog.open(templateRef, {
      height: '420px',
      width: '300px',
    });
  }

  onSubmitAdd(form: NgForm) {
    this.userService.addUser(form.value).subscribe(() => this.updateData(), error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      const errorString: string = JSON.stringify(this.errorBody);
      let messageString = '';
      if (errorString.indexOf('password') !== -1) {
        messageString = messageString + 'The password field must contain at least 6 characters\n ';
      }
      if (errorString.indexOf('email') !== -1) {
        messageString = messageString + 'Please provide a valid email address\n ';
      }
      if (errorString.indexOf('username') !== -1) {
        messageString = messageString + 'The username field must contain at least 6 characters\n ';
      }
      if (messageString.length === 0) {
        swal('Bummer', 'Something went wrong. Please fill the form again carefully', 'error');

      } else {
        swal('Bummer', messageString, 'error');
      }
    });
  }

  onSubmitSendToken(form: NgForm) {
    this.userService.sendToken(form).subscribe(data => swal('Sent!', '', 'success'), error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      swal('Bummer', JSON.stringify(error), 'error');
    });
  }

  removeUser(user: User) {
    this.userService.removeUser(user.userId).subscribe(() => {
    }, error => {
      this.errorBody = error;
      this.receivedException = this.errorBody.error;
      this.updateData();
      swal('Success', this.errorBody.error);
    });
  }

  updateData() {
    this.userService.getUsers()
      .subscribe(data => {
        this.users = data;
      });
  }

}
